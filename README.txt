Installation and Usage
======================
Install the module.
Monitor with select * from mailmon;

Proposed features
=================
* Views integration so you can see the data
* Send a notification if the number of emails sent today exceeds the average
of the last X days by Y%.
It's possible for the date to get confused since the protection against inserting two days of the same value is weak.

